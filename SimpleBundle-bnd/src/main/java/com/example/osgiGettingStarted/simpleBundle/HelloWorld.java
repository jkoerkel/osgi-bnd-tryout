package com.example.osgiGettingStarted.simpleBundle;

public class HelloWorld {

	private String greeting;

	public HelloWorld() {
		super();
	}

	public String getGreeting() {
		return greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

}
