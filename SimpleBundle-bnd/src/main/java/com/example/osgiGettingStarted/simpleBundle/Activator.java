package com.example.osgiGettingStarted.simpleBundle;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator{
	private HelloWorld helloWorld;

	@Override
	public void start(BundleContext arg0) throws Exception {
		helloWorld = new HelloWorld();
		helloWorld.setGreeting("Gradle du Huso");
		System.out.println(helloWorld.getGreeting());
	}

	@Override
	public void stop(BundleContext arg0) throws Exception {
		helloWorld.setGreeting(null);
		System.out.println("Immernoch Huso");
	}
	
}
